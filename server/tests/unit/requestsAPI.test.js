const request = require('request');

describe("Check status of external API", () => {
  it("should return true", () => {
    request('https://hn.algolia.com/api/v1/search_by_date?query=nodejs', function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var data = JSON.parse(body)

        expect(data.hits.length > 0).toBeTruthy();
      }
    });

    // //Testing a boolean
    // expect(forgotPassword()).toBeTruthy();
    // //Another way to test a boolean
    // expect(forgotPassword()).toEqual(true);
  });
});