const mongoose = require('mongoose');

describe("Check status of mongoDB", () => {
  it("should return true", () => {
    mongoose.connect('mongodb://mongo:27017/expressmongo', function(err, db) {
      if (err) throw err;
      db.collection('myData').find().sort({'created_at':-1}).toArray(function(err, results) {
        expect(err).toBeFalsy();
        expect(results.length > 0).toEqual(true);
      });
    });
  });
});