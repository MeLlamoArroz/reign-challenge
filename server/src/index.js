const request = require('request');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const cron = require('node-cron');
const http = require('http');
const path = require('path');
const app = require('express')();

// connect to Mongo daemon
mongoose
  .connect(
    'mongodb://mongo:27017/expressmongo',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

// insert data to my db
request('https://hn.algolia.com/api/v1/search_by_date?query=nodejs', function (error, response, body) {
  if (!error && response.statusCode == 200) {
    var data = JSON.parse(body)

    mongoose.connect('mongodb://mongo:27017/expressmongo', function(err, db) {
      if (err) throw err;
      var myObj = data.hits;
      db.collection('myData').insert(myObj, function(err, res) {
        if (err) throw err;
        db.close();
      });
    });
  }
});

// update data to my db each hour
cron.schedule('0 * * * *', () => {
  console.log('Updated DB')
  request('https://hn.algolia.com/api/v1/search_by_date?query=nodejs', function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var data = JSON.parse(body)

      mongoose.connect('mongodb://mongo:27017/expressmongo', function(err, db) {
        if (err) throw err;
        var myObj = data.hits;
        db.collection('myData').insert(myObj, function(err, res) {
          if (err) throw err;
          db.close();
        });
      });
    }
  });
});

app.set('views', path.join(__dirname, '../client'));
app.set('view engine', 'jade');
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) =>{

  mongoose.connect('mongodb://mongo:27017/expressmongo', function(err, db) {
    if (err) throw err;
    db.collection('myData').find().sort({'created_at':-1}).toArray(function(err, results) {
      if (err){
        console.log("Error")
      }
      else res.send(results);
    });
  });
})


//Post route
app.post('/item/delete', (req, res) => {
  
  mongoose.connect('mongodb://mongo:27017/expressmongo', function(err, db) {
    if (err) throw err;
    db.collection('myData').remove({ 'objectID': req.body.objectID })
  
    console.log("Post successfully removed from polls collection!")
    res.status(200).send()
  });
});

// Server
const port = process.env.PORT || 3001;
const server = http.createServer(app);
server.listen(port, () => console.log(`Server running on port ${port}`));