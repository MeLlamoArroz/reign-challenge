import React from 'react';
import DeleteIcon from '@material-ui/icons/Delete';

import './list-item.scss';

const formatAMPM = (date) => {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

const dateToYMD = (date) => {
  var strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  var d = date.getDate();
  var m = strArray[date.getMonth()];
  var y = date.getFullYear();
  var currentYear = new Date().getFullYear();
  if( currentYear === y)
    return '' + m + ' ' + (d <= 9 ? '0' + d : d);
  else
    return '' + m + ' ' + (d <= 9 ? '0' + d : d) + ' ' + y;
}

const dateFormat = (date,now) => {
  var verify = ''; //'myDate: '+date + 'now: '+now + 'and';
  if(date.getMonth()=== now.getMonth()&& date.getFullYear() === now.getFullYear()) {
    if(date.getDate()=== now.getDate()) {
      return verify + formatAMPM(date);
    }      
    else if(date.getDate()=== now.getDate()-1) {
      return verify + 'Yesterday';
    }
    else {
      return verify + dateToYMD(date);
    }
  }
  else {
    return verify + dateToYMD(date);
  }
}

const CustomListItem = ({ mainTitle, mainAuthor, mainDate, mainId, showDelete, updateList }) => {
  let myDate = new Date(mainDate)
  let now = new Date()

  let handleSubmit = async e => {
    e.stopPropagation();
    e.preventDefault();
    await fetch('http://localhost:3001/item/delete', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ objectID: mainId }),
    });
    updateList(mainId)
  };

  return(
    <div className="flex-container">
      <div>
        <span className="itemTitle">
          { mainTitle }
        </span>
        <span className="itemAuthor">
          {` - ${ mainAuthor } - `}
        </span>
      </div>
      <div className="itemDate">
        { dateFormat(myDate, now) }
      </div>
      <div>
        { showDelete ? <DeleteIcon onClick={(e) => handleSubmit(e)} style={{fill: "#333333"}}/> : null }
      </div>
    </div>
  )
}

export default CustomListItem;