import React, { useState } from 'react';
///List
import ListItem from '@material-ui/core/ListItem';
///

import './row-item.scss';
import CustomListItem from '../list-item/list-item';

const RenderRow = ({ myIndex, myStyle, rawItem, updateList}) => {
  const [isShown, setIsShown] = useState(false);

  return (
    <ListItem button
      className={"list-item-container"}
      style={myStyle}
      key={myIndex}
      onMouseEnter={() => setIsShown(true)}
      onMouseLeave={() => setIsShown(false)}
      onClick={()=> window.open(`${rawItem ? rawItem.story_url === null ? rawItem.url : rawItem.story_url : ''}`)}
    >
      <CustomListItem 
        mainTitle={`${rawItem ? rawItem.story_title === null ? rawItem.title : rawItem.story_title : ''}`}
        mainAuthor={rawItem.author}
        mainDate={rawItem.created_at}
        mainId={rawItem.objectID}
        showDelete={isShown}
        updateList={updateList}
      />
    </ListItem>
  );
}

export default RenderRow