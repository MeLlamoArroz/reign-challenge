import React from 'react';
///List
import { FixedSizeList } from 'react-window';
///

import './App.scss';
import RenderRow from './components/row-item/row-item';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = { 
      apiResponse: ''
    }
  }

  updateList(newItemDeleted) {
    this.setState({
      apiResponse: Object.values(this.state.apiResponse).filter(item => item.objectID !== newItemDeleted) 
    })
  }

  callAPI() {
    fetch('http://localhost:3001/')
      .then(res => res.text() )
      .then(res => this.setState({ apiResponse: JSON.parse(res) }) )
      .catch(err => err)
  }

  componentDidMount() {
    this.callAPI()
  }

  render () {
    return(
      <div className="App">
        <div className="App-header">
          <h1 className="App-title"> HN Feed </h1>
          <h2 className="App-subtitle"> {`We <3 hacker news!`}</h2>
        </div>
        <FixedSizeList height={650} width={"100%"} itemSize={100} itemCount={this.state.apiResponse.length} style={{left: "5%"}}>
          {(props) => 
            <RenderRow
              myIndex={props.index}
              myStyle={props.style}
              rawItem={this.state.apiResponse[props.index]}
              updateList={(id) => this.updateList(id)}
            />}
        </FixedSizeList>
      </div>
    )
  }
}

export default App;