## reign-challenge

Test for reign Full stack developer position.

## Requirements 

You should have docker and docker-compose installed on your machine.

## Installation 

* Clone the project from the repo,
* build your project using

``` 
 $ sudo docker-compose build
 ```
* and/or run the following command directly inside the directory

``` 
 $ sudo docker-compose up -d
 ```
 Your project will run in the browser as

* React / Frontend at 
  + http://localhost:3000/

* API as NodeJS / Backend at 
  + http://localhost:3001/
  




 